var connection = require('../conn'),
    resp = require('./Response'),
    mhs = require('./CekMahasiswa');
const con = require('../conn');
const { Router } = require('express');

exports.index = async function(userId, chatId, msg){
    var message = msg.replace(/Daftar |Bayar |Tiket /igm,'');
    connection.query('SELECT * FROM event where event_name = ?',
    [ message ], 
    async function (err, rows){
        // Kalo Event Gak Sesuai
        if(rows.length === 0){
            resp.index(userId, chatId, 102);
        // Event Sesuai
        }else{
            var id_event = rows[0].id,
                status = rows[0].event_status;
            if(status == 0){
                resp.index(userId, chatId, 405);
            }else{
                mhs.index(userId, chatId, msg, id_event);
            }
        }
    });
}

exports.cek = function(userId, chatId, msg){
    var message = msg.replace(/Daftar /igm,'');
    connection.query(`SELECT * FROM event where event_status = '1'`,
    [ message ], 
    async function (err, rows){
        resp.index(userId, chatId, 501, rows);
    });
}