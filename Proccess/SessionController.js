var cek = require('./CekData'),
    resp = require('./Response'),
    hapus = require('./Daftar/HapusData'),
    connection = require('../conn'),
    save = require('./Daftar/SimpanData'),
    validasi = require('./Daftar/Validasi');

exports.index = async function(userId, chatId, msg, waktu){
    connection.query(`SELECT * FROM mahasiswa WHERE userId = ?`,
    [ userId ], 
    async function (err, rows){
        if(rows.length == 1){
            const   waktuDb = rows[0].timestamp,
                    status = rows[0].status,
                    response = rows[0].response_code,
                    session = waktu - waktuDb;
            if(response <= 140){
                if(session > 300){
                    hapus.index(userId, chatId);
                }else{
                    validasi.index(userId, chatId, msg, rows, waktu);
                }
            }else if(response == 150){
                if(status == null){
                    if(/YA/.test(msg) && msg.length == 2){
                        await save.status(userId, chatId, 1, 152).then(()=>{
                            resp.index(userId, chatId, 152);
                        })
                    }else if(/TIDAK/.test(msg) && msg.length == 5){
                        await hapus.reset(userId, chatId);
                    }else{
                        await resp.index(userId, chatId, 911).then(()=>{
                            resp.index(userId, chatId, 151);
                        })
                    }
                }else{
                    resp.index(userId, chatId, 911);
                }
            }else if(response == 200){
                save.bukti(userId, chatId, msg);
            }else{
                resp.index(userId, chatId, 911);
            }
        }else{
            resp.index(userId, chatId, 911);
        }
    });
    
}