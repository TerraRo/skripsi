var connection = require('../../conn'),
    resp = require('../Response');
const con = require('../../conn');
    simpan = require('./Simpan'),
    apiChatApi = require('../../chat-api');
var moment = require('moment');

var date = moment().format("YYYY-MM-DD");

exports.mhs = async function(userId, chatId, msg, event){
    var message1 = msg.split("\n"),
        nama = message1[0].replace(/Nama = |Nama =|Nama=/igm, ''),
        npm = message1[1].replace(/Npm = |Npm =|Npm=/igm, ''),
        email = message1[2].replace(/Email = |Email =|Email=/igm, ''),
        instansi = message1[3].replace(/Instansi = |Instansi =|Instansi=/igm, ''),
        gender = message1[4].replace(/Gender = |Gender =|Gender=/igm, '');
    connection.query(`UPDATE mahasiswa SET nama = '${nama}', npm = '${npm}', email = '${email}', instansi = '${instansi}', gender = '${gender}', status = '1', timestamp = '${date}', kode_respon = '110' WHERE userId = '${userId}'`, 
    async function (err, result){
        if(err) throw err;
        if(result){
            resp.index(userId, chatId, 170, msg);
        }
    });
}

exports.bayar = async function(userId, chatId, msg, event){
    connection.query(`UPDATE pembayaran SET bukti = '${msg}', kode_respon = '200', validasi = 'PROSES' WHERE userId = '${userId}' AND event = '${event}'`, 
    async function (err, result){
        if(err) throw err;
        if(result){
            resp.index(userId, chatId, 210);
        }
    });
}

exports.mhs1 = async function(userId, chatId, event_number){
    connection.query(`UPDATE mahasiswa SET event = '${event_number}' WHERE userId = '${userId}'`, 
    async function (err, result){
        if(err) throw err;
    });
}

exports.mhs2 = async function(userId, chatId){
    connection.query(`UPDATE mahasiswa SET nama = null, npm = null, email = null, instansi = null, gender = null, status = '1', kode_respon = '100' WHERE userId = '${userId}'`, 
    async function (err, result){
        if(err) throw err;
        if(result){
            resp.index(userId, chatId, 120);
        }
    });
}

exports.mhs3 = async function(userId, chatId, event_number){
    connection.query(`UPDATE mahasiswa SET event = '${event_number}', status = '2', kode_respon = '120' WHERE id = '${userId}'`, 
    async function (err, result){
        if(err) throw err;
    });
}