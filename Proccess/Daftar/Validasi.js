var resp = require('../Response'),
    update = require('../Daftar/Update');

module.exports = {

    index: async function(userId, chatId, msg, event){
        if(!/nama/igm.test(msg) && !/npm/igm.test(msg) && !/Email/igm.test(msg) && !/instansi/igm.test(msg) && !/gender/igm.test(msg)){
            resp.index(userId, chatId, 700);
        }else if(!/nama/igm.test(msg)){
            resp.index(userId, chatId, 701);
        }else if(!/npm/igm.test(msg)){
            resp.index(userId, chatId, 702);
        }else if(!/Email/igm.test(msg)){
            resp.index(userId, chatId, 703);
        }else if(!/instansi/igm.test(msg)){
            resp.index(userId, chatId, 704);
        }else if(!/gender/igm.test(msg)){
            resp.index(userId, chatId, 705);
        }else{
            var message1 = msg.split("\n"),
                nama = message1[0].replace(/Nama = |Nama =|Nama=/igm, ''),
                npm = message1[1].replace(/Npm = |Npm =|Npm=/igm, ''),
                email = message1[2].replace(/Email = |Email =|Email=/igm, ''),
                instansi = message1[3].replace(/Instansi = |Instansi =|Instansi=/igm, ''),
                gender = message1[4].replace(/Gender = |Gender =|Gender=/igm, '');
            if(message1.length > 5){
                resp.index(userId, chatId, 706);
            }else if(nama.length == 0){
                resp.index(userId, chatId, 707);
            }else if(npm.length == 0){
                resp.index(userId, chatId, 708);
            }else if(email.length == 0){
                resp.index(userId, chatId, 709);
            }else if(instansi.length == 0){
                resp.index(userId, chatId, 710);
            }else if(gender.length == 0){
                resp.index(userId, chatId, 711);
            }else{
                if(/[0-9!@#$%^&*\[\]`~\-_+=';(),.?"/:{}|<>]/i.test(nama) || nama.length > 25){
                    resp.index(userId, chatId, 712);
                }else if(/[A-Z!@#$%^&*\[\]`~\-\_\+\=';(),.?"\\/:{}|<>]/i.test(npm) || npm.length > 15){
                    resp.index(userId, chatId, 713);
                }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)){
                    resp.index(userId, chatId, 714);
                }else if(/[0-9!@#$%^&*\[\]`~\-\_\+\=';(),.?"\\/:{}|<>]/i.test(instansi) || instansi.length > 30){
                    resp.index(userId, chatId, 715);
                }else if(!/P|L/i.test(gender) || gender.length > 1){
                    resp.index(userId, chatId, 716);
                }else{
                    update.mhs(userId, chatId, msg, event);
                }
            }
        }
    }
}