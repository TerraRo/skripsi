var connection = require('../conn'),
    resp = require('./Response'),
    simpan = require('./Daftar/Simpan'),
    update = require('./Daftar/Update');

exports.index = async function(userId, chatId, msg, event, type, array){
    connection.query('SELECT * FROM pembayaran WHERE userId = ? AND event = ?',
    [ array[0].id, event ], 
    async function (err, rows){
        console.log(event)
        // Kalo Mahasiswa belum terdaftar di event
        if(rows.length === 0){
            simpan.trans1(userId, chatId, event, array)
        // Mahasiswa sudah terdaftar
        }else{
            var status = rows[0].status,
                bukti = rows[0].bukti,
                qr = rows[0].qr,
                eve = rows[0].kode_respon;
            if(status == 1){
                if(/Daftar/igm.test(msg)){
                    resp.index(userId, chatId, 160)
                }else if(/Bayar/igm.test(msg)){
                    resp.index(userId, chatId, 200, event)
                }else if(/Tiket|Info/igm.test(msg)){
                    resp.index(userId, chatId, 201)
                }else{
                    if(type == 'image'){
                        if(bukti == null){
                            update.bayar(userId, chatId, msg, event);
                        }else{
                            resp.index(userId, chatId, 211)
                        }
                    }else{
                        console.log('halo')
                        resp.index(userId, chatId, 404)
                    }
                }
            }else if(status == 2){
                if(/Daftar/igm.test(msg)){
                    resp.index(userId, chatId, 180)
                }else if(/Bayar/igm.test(msg)){
                    resp.index(userId, chatId, 221)
                }else if(/Tiket/igm.test(msg)){
                    resp.index(userId, chatId, 300, qr, event)
                }else if(/Info/igm.test(msg)){
                    resp.index(userId, chatId, 400, rows, array)
                }else{
                    resp.index(userId, chatId, 404)
                }
            }else{
                console.log('udahan')
                resp.index(userId, chatId, 404)
            }
        }
    });
}