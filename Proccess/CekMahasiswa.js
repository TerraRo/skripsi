var connection = require('../conn'),
    resp = require('./Response'),
    simpan = require('./Daftar/Simpan'),
    trans = require('./transaksi'),
    update = require('./Daftar/Update'),
    validasi = require('./Daftar/Validasi');

exports.index = async function(userId, chatId, msg, id_event, type){
    connection.query('SELECT * FROM mahasiswa where userId = ?',
    [ userId ], 
    async function (err, rows){
        // Kalo Mahasiswa belum terdaftar di sistem
        if(rows.length === 0){
            if(/Daftar/igm.test(msg)){
                simpan.index(userId, chatId, id_event);
            }else if(/Bayar|Tiket|Info/igm.test(msg)){
                resp.index(userId, chatId, 101)
            }
        // Mahasiswa sudah terdaftar
        }else{
            var kode = rows[0].kode_respon,
                event = rows[0].event,
                user = rows[0].id,
                status = rows[0].status;
            // Proses Regis SEMA
            if(status == 1){
                if(kode == 100){
                    validasi.index(userId, chatId, msg, event);
                }else if(110){
                    if(/YA/igm.test(msg)){
                        //resp.index(userId, chatId, 130);
                        simpan.trans1(userId, chatId, event, rows);
                    }else if(/TIDAK/igm.test(msg)){
                        update.mhs2(userId, chatId)
                    }else{
                        resp.index(userId, chatId, 404);
                    }
                }
            // Proses yang lainnya
            }else if(status == 2){
                if(event == id_event){
                    trans.index(userId, chatId, msg, event, type, rows);
                }else{
                    trans.index(userId, chatId, msg, event, type, rows);
                }
            }
        }
    });
}