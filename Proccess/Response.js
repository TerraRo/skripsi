var apiChatApi = require('../chat-api'),
    qrcode = require('qrcode-generator'),
    QRCode = require('qrcode'),
    connection = require('../conn');

exports.index = async function(userId, chatId, code, array, data) {
    connection.query(`SELECT * FROM response WHERE status = ?`,
    [ code ],
    async function (error, rows){
            var resp = rows[0].status,
                msg = rows[0].message;
            if(resp == 100 || resp == 140 || resp == 200){
                connection.query(`SELECT * FROM event WHERE id = ?`,
                [ array ],
                async function ahoy(error, result){
                    var msg1 = msg.replace(/-event-/gi, result[0].event_name);
                    console.log(msg1)
                    await apiChatApi('sendMessage', {chatId: chatId, body: msg1});
                });
            }else if(resp == 170){
                var message1 = array.split("\n"),
                    nama = message1[0].replace(/Nama = |Nama =|Nama=/igm, ''),
                    npm = message1[1].replace(/Npm = |Npm =|Npm=/igm, ''),
                    email = message1[2].replace(/Email = |Email =|Email=/igm, ''),
                    instansi = message1[3].replace(/Instansi = |Instansi =|Instansi=/igm, ''),
                    gender = message1[4].replace(/Gender = |Gender =|Gender=/igm, '');
                var msg1 = msg.replace(/-nama-/igm, nama),
                    msg2 = msg1.replace(/-npm-/igm, npm),
                    msg3 = msg2.replace(/-email-/igm, email),
                    msg4 = msg3.replace(/-instansi-/igm, instansi),
                    msg5 = msg4.replace(/-gender-/igm, gender);
                console.log(msg5)
                await apiChatApi('sendMessage', {chatId: chatId, body: msg5});
            }else if(resp == 300){
                var opts = {
                    errorCorrectionLevel: 'H',
                    type: 'image/jpeg',
                    quality: 0.9,
                  }
                connection.query(`SELECT * FROM event WHERE id = ?`,
                [ data ],
                async function ahoy(error, result){
                    QRCode.toDataURL(`${array}`, opts, async function (err, url) {
                        var msg1 = msg.replace(/-event-/igm, result[0].event_name);
                        console.log(msg1)
                        await apiChatApi('sendFile', {chatId: chatId, body: url, filename: 'qr.png', caption: msg1});
                    })
                });
            }else if(resp == 400){
                var nama = data[0].nama,
                    npm = data[0].npm,
                    email = data[0].email,
                    instansi = data[0].instansi,
                    gender = data[0].gender;
                var fina = msg.split('#')
                var msg1 = fina[0].replace(/-nama-/gi, nama),
                    msg2 = msg1.replace(/-npm-/gi, npm),
                    msg3 = msg2.replace(/-email-/gi, email),
                    msg4 = msg3.replace(/-instansi-/gi, instansi),
                    msg5 = msg4.replace(/-gender-/gi, gender);
                //await apiChatApi('sendMessage', {chatId: chatId, body: msg5});
                var user = data[0].id;
                connection.query(`SELECT * FROM event WHERE id = ?`,
                [ data[0].event ],
                async function ahoy(error, result){
                    connection.query(`SELECT * FROM pembayaran WHERE userId = ?`,
                    [ user ],
                    async function ahoy(error, rows1){
                        var msg11 = '';
                        for(let i=0; i<rows1.length; i++){
                            var event = rows1[i].event,
                                status = rows1[i].status,
                                validasi = rows1[i].validasi;
                            var msg6 = fina[1].replace(/-event-/gi, result[0].event_name),
                                msg7 = msg6.replace(/-status-/gi, status),
                                msg8 = msg7.replace(/-validasi-/gi, validasi),
                                msg9 = msg8.replace(/1/gi, 'TERDAFTAR'),
                                msg10 = msg9.replace(/2/gi, 'LUNAS');
                            msg11 = msg10.replace(/null/gi, 'BELUM');
                            //await apiChatApi('sendMessage', {chatId: chatId, body: msg11});
                            console.log(msg11)
                        }
                        await apiChatApi('sendMessage', {chatId: chatId, body: msg5+msg11});
                    });
                });
            }else if(resp == 500){
                var msg1 = msg.replace(/-Deskripsi-/gi, array);
                console.log(msg1)
                await apiChatApi('sendMessage', {chatId: chatId, body: msg1});
            }else if(resp == 501){
                var message = '',
                    msg1 = msg.split('#');
                for(let i=0; i<array.length; i++){
                    var name = array[i].event_name,
                        cost = array[i].event_cost;
                    message = msg1[1].replace(/-event_name-/gi, name);
                    message = message.replace(/-event_cost-/gi, cost);
                    console.log(msg1[0]);
                }
                console.log(message);
                //await apiChatApi('sendMessage', {chatId: chatId, body: msg1});
            }else{
                console.log(msg)
                await apiChatApi('sendMessage', {chatId: chatId, body: msg});
            }
    });
};