var download = require('download-file')
 
module.exports = async function(url, caption){
    console.log(url)
    var options = {
        directory: "./Images",
        filename: caption+".gif"
    }
    
    download(url, options, function(err){
        if (err) throw err
        console.log(caption)
    })
}