process.on('unhandledRejection', err => {
    console.log(err)
});	

var event = require('./Proccess/CekEvent'),
    resp = require('./Proccess/Response'),
    mhs = require('./Proccess/CekMahasiswa');

module.exports = function(app){

    app.get('/', async function (req, res) {
        res.send("It's working V1.5");
    }); 

    app.post('/webhook', async function (req, res) {
        var data = req.body,
            instance = data.instanceId;
        
        // Get Information from sender
        for (var i in data.messages) {
            var author = data.messages[i].author,
                body = data.messages[i].body,
                chatId = data.messages[i].chatId,
                senderName = data.messages[i].senderName,
                id = data.messages[i].id,
                type = data.messages[i].type,
                time = data.messages[i].time,
                chatName = data.messages[i].chatName,
                caption = data.messages[i].caption,
                fromMe = data.messages[i].fromMe;
            
            // Check the sender
            if(fromMe)return;

            // Uppercase the messages text
            const msg = body.toUpperCase();
            const userId = author.slice(0, -5);

            // Check type of messages
            if(type=='chat'){
                if(/DAFTAR|TIKET/.test(msg)){

                    event.index(userId, chatId, msg);

                }else if(/BAYAR|INFO/.test(msg)){

                    mhs.index(userId, chatId, msg);

                }else if(/EVENT/.test(msg)){

                    event.cek(userId, chatId, msg);

                }else if(/HELP/.test(msg)){

                    resp.index(userId, chatId, 911);

                }else{

                    mhs.index(userId, chatId, msg);

                }
            }else if(type=='image'){

                mhs.index(userId, chatId, body, null, type);

            }
        }

        res.send('Ok');

    });
}