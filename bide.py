from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

import threading
import time
import mysql.connector

# import database
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database="bidebackend"
)

mycursor = mydb.cursor()

# Variable global
driver = None

# fungsi untuk menampilkan web whatsapp
def main():
    global driver

    chrome_options = Options() # Saving the last session
    chrome_options.add_argument("user-data-dir=selenium")
    driver = webdriver.Chrome('C:/chromedriver', options=chrome_options)
    driver.get('https://web.whatsapp.com/')

    show_menu()

# fungsi untuk menampilkan event
def event():
    msg_box = driver.find_element_by_xpath('//div[@class="_1Plpp"]')
    msg_box.send_keys("Ini beberapa event yang sedang dimulai")

    down_button = driver.find_element_by_xpath('//span[@class="_2pyvj"]')
    down_button.click()

    button = driver.find_element_by_class_name('_35EW6')
    button.click()

    scroll = driver.find_elements_by_xpath("//div[@class='_9tCEa']")
    for list in scroll :
        driver.execute_script("arguments[0].scrollIntoView();", list)

# fungsi untuk daftar
def unregistered(nomor):

    msg_box = driver.find_element_by_xpath('//div[@class="_1Plpp"]')
    msg_box.send_keys("Anda belum mendaftar " + nomor + "\n Jika ingin mendaftar isi data diri dengan format 'daftar: nama, email' \n contoh daftar: tengku, ta@gmail.com")

    down_button = driver.find_element_by_xpath('//span[@class="_2pyvj"]')
    down_button.click()

    button = driver.find_element_by_class_name('_35EW6')
    button.click() 

    recentList = driver.find_elements_by_xpath("//div[@class='_9tCEa']")
    for list in recentList :
        driver.execute_script("arguments[0].scrollIntoView();", list)

# fungsi untuk daftar
def registered(company):

    msg_box = driver.find_element_by_xpath('//div[@class="_1Plpp"]')
    msg_box.send_keys("You are going to book a reservation at " + company[1] + ", Please fill number of customer(s)")

    down_button = driver.find_element_by_xpath('//span[@class="_2pyvj"]')
    down_button.click()

    button = driver.find_element_by_class_name('_35EW6')
    button.click() 

    recentList = driver.find_elements_by_xpath("//div[@class='_9tCEa']")
    for list in recentList :
        driver.execute_script("arguments[0].scrollIntoView();", list)

# fungsi cek nomor
def cekNomor(nomor, id_company):
    cek1 = "SELECT * FROM bide_company WHERE id = %s"
    id = (id_company,)
    mycursor.execute(cek1, id)
    myresult1 = mycursor.fetchone()

    # print(myresult1)

    if(myresult1):
        cek = "SELECT * FROM bide_user WHERE phone_number = %s"
        no = (nomor,)
        mycursor.execute(cek, no)
        myresult = mycursor.fetchone()

        if not myresult:
            tambah = """INSERT INTO bide_user (phone_number) VALUES (%s)"""
            nilai = (nomor,)
            mycursor.execute(tambah, nilai)
            mydb.commit()

            print(mycursor.rowcount, "record inserted.")
        else:
            pass

        registered(myresult1)
    else:
        pass
    #     # registered(nomor)
    #     print("tidak ada")

# fungsi untuk cek jadwal
def cekjadwal():
    msg_box = driver.find_element_by_xpath('//div[@class="_1Plpp"]')
    msg_box.send_keys("Ini untuk cek jadwal")

    down_button = driver.find_element_by_xpath('//span[@class="_2pyvj"]')
    down_button.click()

    button = driver.find_element_by_class_name('_35EW6')
    button.click() 

    recentList = driver.find_elements_by_xpath("//div[@class='_9tCEa']")
    for list in recentList :
        driver.execute_script("arguments[0].scrollIntoView();", list)

# mengulang proses pesan
def repeat():
    down_button = driver.find_element_by_xpath('//span[@class="_2pyvj"]')
    down_button.click()

    recentList = driver.find_elements_by_xpath("//div[@class='_9tCEa']")
    for list in recentList :
        driver.execute_script("arguments[0].scrollIntoView();", list)

# fungsi untuk cek pesan
def show_menu():

    global driver

    pesan = None
    # i = 0
    
    while(True):
        try:
            pesan_masuk = driver.find_element_by_class_name('_2EXPL.CxUIE')
            if(pesan_masuk != None):
                pesan_masuk.click()

                pesan = driver.find_elements_by_class_name("_3zb-j")
                pesanBaru = pesan[-1].text

                cariNomor = driver.find_elements_by_class_name("_3XrHh")
                nomor = cariNomor[-1].text

                recentList = driver.find_elements_by_xpath("//div[@class='_9tCEa']")
                for list in recentList :
                    driver.execute_script("arguments[0].scrollIntoView();", list)

                if(pesanBaru.find('halo')):
                    teks = pesanBaru.split(" ")
                    cekNomor(nomor, teks[1])
                # elif(pesanBaru.find('customer') or pesanBaru.find('people')):
                #     teks = pesanBaru.split(" ")
                #     cekNomor(nomor, teks[1])
                elif(pesanBaru.upper() == "EVENT"):
                    event()
                elif(pesanBaru.upper() == "CEK JADWAL"):
                    cekjadwal()
                else:
                    repeat()
            else:
                pass
        except (NoSuchElementException, StaleElementReferenceException) as e:
            pass

if __name__ == "__main__":
    main()