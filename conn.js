var mysql = require('./node_modules/mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "skripsi"
});

con.connect(function (err){
    if(err) throw err;
});

module.exports = con;